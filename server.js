const grpc = require("@grpc/grpc-js");
const protoLoader = require("@grpc/proto-loader");
const PROTO_PATH = "./animation.proto";

const packageDefinition = protoLoader.loadSync(PROTO_PATH);
const animationFramesProto = grpc.loadPackageDefinition(packageDefinition);

const server = new grpc.Server();
const animation = {
  frames: 1,
  fps: 60,
  path: 'path/to/animations/frames/'
};

server.addService(animationFramesProto.AnimationFramesService.service, {
  getAnimationFrames: async (req, callback) => {
    console.log('Generating frames...')
    await new Promise((res) => setTimeout(res, 3000))
    console.log('Finished generating frames')
    callback(null, animation);
  },
});

server.bindAsync(
  "127.0.0.1:50051",
  grpc.ServerCredentials.createInsecure(),
  (error, port) => {
    console.log("Server running at http://127.0.0.1:50051");
    server.start();
  }
);