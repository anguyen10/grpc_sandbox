const grpc = require("@grpc/grpc-js");
const protoLoader = require("@grpc/proto-loader");
const PROTO_PATH = "./animation.proto";

const packageDefinition = protoLoader.loadSync(PROTO_PATH);

const AnimationFramesService = grpc.loadPackageDefinition(packageDefinition).AnimationFramesService;

const client = new AnimationFramesService(
  "localhost:50051",
  grpc.credentials.createInsecure()
);

client.getAnimationFrames({}, (error, videos) => {
  console.log(error, videos)
});

module.exports = client;